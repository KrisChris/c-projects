/**
 * @file common.h
 * @author Krystian Krol
 * @brief contains definitions and imports used in the whole project
 */
#ifndef COMMON_H
#define COMMON_H

/* standard libraries to use everywhere in the code */
#include <sys/types.h>  
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include "khash.h"
#ifdef TRUE
#undef TRUE
#endif

#ifdef FALSE
#undef FALSE
#endif

typedef enum { FALSE, TRUE } boolean;
#define MAX_BUF 2048*4
#define ENOUGH ((CHAR_BIT * sizeof(int) - 1) / 3 + 2)
KHASH_MAP_INIT_INT(32, char*)
KHASH_MAP_INIT_INT(64, uint32_t)
#endif /* COMMON_H */
