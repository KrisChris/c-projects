/**
 * @file server.c
 * @author Krystian Krol
 * @brief contains implementation of the user programs server
 * 
 * This server is designed to monitor registered hosts. 
 * Each host registers with a list of currently running processes, unique numeric id and
 * time interval between updates. 
 * The server checks if the clients (hosts) meet their declared deadlines
 * for sending in updates. The server also verifies each time if the update contains the
 * same list of processes as sent on registration.
 */

#include <signal.h>
#include <syslog.h>
#include "common.h"
#include "networking.h"
#include "concurrency.h"
#include "user.h"

#define SERVICE "test1"
#define PIDF "/tmp/test1.pid"
#define MSG_ERROR "malformed message\n"
#define ROK "rok\n"
#define RERR "rerr\n"
#define UOK "uok\n"
#define UERR "uerr\n"
static boolean LOG = FALSE;
static boolean SHUTDOWN = FALSE;

/* container for arguments passed to the executor */
struct s_context{
  struct u_context *uCont;
  struct e_context *eCont;
};


/**
 * @brief generic function for use by the concurrency framework 
 * 
 * Handles incoming messages 
 */
void worker_fun(void*, int);

/**
 * @brief handles the message sent by the client
 * 
 * There are two types of message formats: 
 * r user_id(int) time_interval(int) prog1 prog2 ..\n 
 * u user_id(int) prog1 prog2
 * 
 * This function handles each case; registers a new user 
 * or handles the update
 */
void handle_msg(struct u_context*,char*, int);

/**
 * @brief verfies if the message received from a client is valid
 * 
 * The message should begin with 'u' or with 'r'
 * for update or register message, respectively.
 * A message must be terminated with a newline sign: '\n'
 * @return TRUE if the message meets the specification
 */
boolean is_valid_message(char*, size_t);

void 
worker_fun(void *p, int fd) 
{
  struct s_context *big_one = (struct s_context*)p;
  struct e_context *eCont = big_one->eCont;
  struct u_context *uCont = big_one->uCont;

  errno = 0;
  int lfd = eCont->lfd, cfd;
  if(fd == lfd) {
    cfd = accept(lfd, NULL, NULL); /* accepting the pending connection ;) */
    if (cfd == -1) {
      perror("accept");
      /* log  error somehow */
      syslog(LOG_INFO, "accept: %s", strerror(errno));
      if (set_blocking(cfd, FALSE) == FALSE)
	perror("set to nonblocking fail");
    } else{
      e_add(eCont, cfd);
    }
  }else {
    int s;
    char buf[MAX_BUF];
    memset(buf, 0, sizeof(char)*MAX_BUF);
    s = read(fd, buf, MAX_BUF);
    if( s == -1) {
      perror("socket reading error");
      if (LOG) syslog(LOG_INFO, "read: %s", strerror(errno));
      e_drop(eCont, fd); /* disconnecting and cleaning up in e_context */
    } else if (s == 0) {
      perror("the other end hung up unexpectedly");
      if (LOG) syslog(LOG_INFO, "peer connection lost");
      e_drop(eCont, fd);
    } else if (s == MAX_BUF && buf[s-1] != '\n') {
      perror("buffer full, dropping packets");
      if (LOG) syslog(LOG_INFO, "buffer full, dropping packets");
    }else{
      if (is_valid_message(buf, s)) {
	handle_msg(uCont, strdup(buf), fd);
      } else {
	e_buff(eCont, fd, buf, s);
	if (get_buff(eCont, fd, is_valid_message, buf, sizeof(char)*MAX_BUF) == TRUE) {
	  printf("complete read: %s\n", buf);
	  handle_msg(uCont, strdup(buf), fd);
	}
      } 
  }
  }
} /* worker_fun */



void
handle_msg(struct u_context *c, char *msg, int fd) {
  char *orig = msg;
  char *copy = orig;
  switch (copy[0]) {
  case 'r': {
    char *time;
    uint32_t id;
    strsep(&copy, " ");
    errno = 0;
    id = strtol(strsep(&copy, " "), NULL, 10);
    if (errno) {
      if (LOG) syslog(LOG_INFO, "wrong id format");
      write(fd, RERR, strlen(RERR));
    }
    time = strsep(&copy, " ");
    if( id && time && copy) {
      errno = 0;
      time_t val = strtol(time, NULL, 10);
      if (errno) {
	perror("strtol");
	write(fd, RERR, strlen(RERR));
	if (LOG) syslog(LOG_INFO, "wrong time format from id: %d", id);
	break;
      }
      if(u_register(c, id, copy, val) == TRUE) {
	write(fd, ROK, strlen(ROK));
	if (LOG) syslog(LOG_INFO, "ROK registration success id: %d", id);
	break;
      }
    }
    write(fd, RERR, strlen(RERR));
    if (LOG) syslog(LOG_INFO, "unsuccessful register");
    break;
  }
  case 'u': {
    uint32_t id;
    strsep(&copy, " ");
    errno = 0;
    id = strtol(strsep(&copy, " "), NULL, 10);
    if (errno) {
      if (LOG) syslog(LOG_INFO, "sent wrong non-numeric id");
    }
    if (id && copy) {
      if( u_verify(c, id, copy) == TRUE) {
	write(fd, UOK, strlen(UOK));
	if (LOG) syslog(LOG_INFO, "sent UOK to id: %d", id);
	break;
      }
    }
    write(fd, UERR, strlen(UERR));
    if (LOG) syslog(LOG_INFO, "sent UERR to id: %d", id);
    break;
  }
  default: {
    write(fd, MSG_ERROR, strlen(MSG_ERROR));
    if (LOG) syslog(LOG_INFO, "received malformed msg");
  }
  }
  free(orig);
} /* handle_msg */


boolean
is_valid_message(char *buf, size_t len)
{
  if (len == 0 || buf == NULL ||  strlen(buf) == 0) return FALSE;
  if(buf[0] == 'u' || buf[0] == 'r')
    if(buf[len-1] == '\n')
      return TRUE;
  return FALSE;
} /* complete_msg */

static void
server_shutdown(int signal){
  SHUTDOWN = TRUE;
 
} /* server_shutdown */

static void 
server_setup(boolean demonize){
  signal(SIGUSR1, server_shutdown);
  if (demonize) daemon(0,0);
  if (LOG) {
    openlog(SERVICE, LOG_PID, LOG_USER);
    syslog(LOG_INFO,"%s started", SERVICE);
  }
  int pid = getpid();
  FILE *fp = fopen(PIDF, "w+");
  if (fp == NULL) {
    perror("fopen");
    if (LOG) syslog(LOG_INFO, "fopen: %s", strerror(errno));
    server_shutdown(0);
  }
  fprintf(fp, "%d\n", pid);
  fclose(fp);
} /* server_setup */

static const char* usage = \
"-d run in daemon mode (default: FALSE)\n"\
"-l enable logging (FALSE)"\
"-i [value] time in second between sanity checks (default: 10 s)\n"\
"-h print help\n";

/**
 * @brief main entry point of the program
 * @argv 
 * -d run in daemon mode (default: FALSE)
 * -l enable logging (FALSE)
 * -i [value] time in second between sanity checks (default: 10 s)
 * -h print help
 */
int
main(int argc, char **argv)
{
  boolean demonize = FALSE;
  time_t last = 0, interval = 5;
  int c;
  while((c = getopt(argc, argv, "hldi:")) != -1) {
    switch (c)
      {
      case 'l':
	LOG = TRUE;
	break;
      case 'd':
	demonize = TRUE;
	break;
      case 'i':
	interval = atoi(optarg);
	break;
      case 'h':
	fprintf(stderr, "%s", usage);
	return 1;
      default:
	fprintf(stderr, "%s", usage);
	abort();
      }
  }
  struct s_context sCont;
  struct e_context eCont;
  struct u_context uCont;
  int lfd;
  memset(&eCont, 0, sizeof(struct e_context));
  memset(&uCont, 0, sizeof(struct u_context));
  sCont.eCont = &eCont;
  sCont.uCont = &uCont;

  server_setup(demonize);

  e_init(&eCont);
  u_init(&uCont);
  lfd = net_listen(SERVICE, 100, NULL);
  if (lfd == -1) {
    perror("listen");
    if (LOG) syslog(LOG_INFO, "listen: %s", strerror(errno));
  }
  eCont.lfd = lfd; 
  e_add(&eCont, lfd);
 
  /* supports graceful shutdown */
  while (!SHUTDOWN) {
    time_t now = time(0);
    handle_active_clients(&eCont, worker_fun, (void*)&sCont, interval*1000);
    if ((now - last) >= interval) {
      last = now;
      u_checkpoint(&uCont);
    }
  }

  if (LOG) {
    syslog(LOG_INFO,"%s finished", SERVICE);
    closelog();
  }
  remove(PIDF);
  for(khiter_t k = kh_begin(eCont.fd_buf); k != kh_end(eCont.fd_buf); ++k)
    if (kh_exist(eCont.fd_buf, k)) free(kh_value(eCont.fd_buf, k));
  kh_destroy(32, eCont.fd_buf);
  kh_destroy(64, eCont.fd_off);
  u_destroy(&uCont);
  return 0;
} /* main */


