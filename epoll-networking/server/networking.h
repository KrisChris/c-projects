/**
 * @file networking.h
 * @author Krystian Krol
 * @brief header file for networking related declarations
 */
#ifndef NETWORKING_H
#define NETWORKING_H

#include <sys/socket.h>
#include <netdb.h>

/**
 * @brief used to connect to the @service running at the given @host
 * @type could be SOCK_STREAM or SOCK_DGRAM
 * @return file descriptor to use in this connection, -1 if failed
 */
int net_connect(const char *host, const char *service, int type);

/**
 * @brief listen for incoming TCP connections
 * @service name of service registered in /etc/services
 * @backlog number of buffered incoming connections 
 * @return file descriptor for listening, or -1 if failed
 */
int net_listen(const char *service, int backlog, socklen_t *addrlen);

/**
 * @brief for UDP clients & servers
 * @type could be SOCK_STREAM or SOCK_DGRAM actually
 * @return a file descriptor for the new socket
 */
int net_bind(const char *service, int type, socklen_t *addrlen);

boolean set_blocking(int fd, boolean blocking);

#endif /* NETWORKING_H */
