/**
 * @file user.c
 * @author Krystian Krol
 * @brief contains definitions of functions declared in user.h
 */
#include "user.h"

boolean
u_register(struct u_context *c, uint32_t id, char *progs, time_t timeout)
{
  int ret;
  khiter_t k;
  
  k = kh_put(32, c->uid_programs, id, &ret);
  if (!ret) return FALSE;

  kh_value(c->uid_programs, k) = strdup(progs);
  k = kh_put(64, c->uid_timeout, id, &ret);
  kh_value(c->uid_timeout, k) = timeout;
  k = kh_put(64, c->uid_checkpoint, id, &ret);
  kh_value(c->uid_checkpoint, k) = time(0);
  return TRUE;
} /* u_register */

void 
u_init(struct u_context *p)
{
  p->uid_programs = kh_init(32);
  p->uid_timeout = kh_init(64);
  p->uid_checkpoint = kh_init(64);
}

void
u_destroy(struct u_context *p)
{
  for(khiter_t k = kh_begin(p->uid_programs); k != kh_end(p->uid_programs); ++k)
    if (kh_exist(p->uid_programs, k)) free(kh_value(p->uid_programs, k));
  kh_destroy(32, p->uid_programs);

  kh_destroy(64, p->uid_timeout);
  kh_destroy(64, p->uid_checkpoint);
}

boolean
u_verify(struct u_context *c, uint32_t id, char *new_progs)
{
  time_t now;
  khiter_t k;
  now = time(0);
  k = kh_get(64, c->uid_checkpoint, id);
  if (k == kh_end(c->uid_checkpoint)) return FALSE;
  kh_value(c->uid_checkpoint, k) = now;
  k = kh_get(32, c->uid_programs, id);
  if (k == kh_end(c->uid_programs)) return FALSE;
  char *progs = kh_value(c->uid_programs, k);
  if (progs == NULL) return FALSE;
  return strcmp(progs, new_progs) == 0 ? TRUE : FALSE;
} /* u_verify */

void
u_checkpoint(struct u_context *c)
{
  khiter_t ktm, k;
  time_t now = time(0);
  for(ktm = kh_begin(c->uid_timeout); ktm != kh_end(c->uid_timeout); ++ktm){
      if (kh_exist(c->uid_timeout, ktm)) {
        const uint32_t id = kh_key(c->uid_timeout, ktm);
	time_t deadline = kh_value(c->uid_timeout, ktm);

	k = kh_get(64, c->uid_checkpoint, id);
	time_t last = kh_value(c->uid_checkpoint, k);
	if ((now - last) > deadline) {
	  fprintf(stderr, "client id %d: %ld s since last checkpoint\n",id, now-last);
	}
      }
  }
} /* u_checkpoint */
