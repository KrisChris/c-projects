/**
 * @file concurrency.c
 * @author Krystian Krol
 * @brief contains definitions for concurrency related functions
 */
#include <sys/epoll.h>
#include <fcntl.h>
#include "concurrency.h"

int 
e_init(struct e_context *context)
{
  int epfd;
  epfd = epoll_create(1); /* not relevant in current implementation */
  if (epfd == -1)
    return -1; /*  */
  context->epfd = epfd;
  context->fd_buf = kh_init(32);
  context->fd_off = kh_init(64);
  return 0;
}

int
e_add(struct e_context *context, int fd)
{
  int res = 0;
  struct epoll_event ev;
  memset(&ev, 0, sizeof(struct epoll_event));
  ev.events = EPOLLIN | EPOLLRDHUP; /*watch readiness to read / disconnect */
  ev.data.fd = fd;
  char *buf = (char*)calloc(MAX_BUF, sizeof(char));

  khiter_t k = kh_put(32, context->fd_buf, fd, &res);
  kh_value(context->fd_buf, k) = buf;
  k = kh_put(64, context->fd_off, fd, &res);
  kh_value(context->fd_off, k) = 0;
  return epoll_ctl(context->epfd, EPOLL_CTL_ADD, fd, &ev);
}


void
e_buff(struct e_context *c, int fd, char* str, size_t len)
{
  size_t off;
  char *buff;
  khiter_t k = kh_get(32, c->fd_buf, fd);
  buff = kh_value(c->fd_buf, k);
  k = kh_get(64, c->fd_off, fd);
  off = kh_value(c->fd_off, k);
  if ( off + len <= MAX_BUF) {
    memcpy(buff+off, str, len);
    kh_value(c->fd_off, k) = off+len;
  } else {
    perror("buffer size exceeded in e_buff, flushing buffer");
    kh_value(c->fd_off, k) = 0;
    memset(buff, 0, sizeof(char)*MAX_BUF);
  }
}

boolean
get_buff(struct e_context *c, int fd, boolean (*f)(char*, size_t), 
char *consumer, size_t len)
{
  size_t offset;
  char *candidate;
  khiter_t k = kh_get(32, c->fd_buf, fd);
  candidate = kh_value(c->fd_buf, k);
  k = kh_get(64, c->fd_off, fd);
  offset = kh_value(c->fd_off, k);
  if ((*f)(candidate, offset) == TRUE) {
    snprintf(consumer,len, "%s", candidate);
    memset(candidate, 0, sizeof(char)*MAX_BUF);
    kh_value(c->fd_off, k) = 0;
    return TRUE;
  }
  return FALSE;
}

void 
e_drop(struct e_context* context, int fd)
{
  if(close(fd) == -1)
    perror("closing dead socket: ");
  khiter_t k = kh_get(32, context->fd_buf, fd);
  free(kh_value(context->fd_buf,k));
  kh_del(32, context->fd_buf, k);
}

int
handle_active_clients(struct e_context* context, void (*f)(void*, int), void* big_context, int timeout)
{
  int ready, handled = 0, fd;
  /* waiting  */

  ready = epoll_wait(context->epfd, context->evlist, MAX_EVENTS, timeout);
  if (ready == -1) {
    if (errno == EINTR) {
       /* interrupted by signal, restarting */
      return 0;
    }
    else
      return -1; /* error found, return the info */
  }
  for (int i = 0; i < ready; ++i) {
    fd = context->evlist[i].data.fd;
    if (context->evlist[i].events & EPOLLIN) {
	(*f)(big_context, fd); /* we serve the ready socket */
      handled++;
    } else if(context->evlist[i].events & (EPOLLRDHUP | EPOLLHUP | EPOLLERR)) {
      /* we close the dead socket and perform clean up */
      e_drop(context, fd);
    }
  }
  return handled; /* return how many were active in this pass */
}
