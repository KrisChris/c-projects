/**
 * @file user.h
 * @author Krystian Krol
 * @brief header file containing prototypes of user-handling functions
 */

#ifndef USER_H
#define USER_H
#include "common.h"

/**
 * @brief struct storing globally used user data
 */
struct u_context{
  khash_t(32) *uid_programs; /**< map from user id (int) to their list of registered programs (char*) */
  khash_t(64) *uid_timeout; /**< map from uid to their registered timeouts */
  khash_t(64) *uid_checkpoint; /**< map from uid to their last checkpoint (int, seconds) */
};

/** 
 * @brief initializes the u_context struct with default values
 */
void u_init(struct u_context*);

/** 
 *  @brief destructor for u_context 
 *  
 */
void u_destroy(struct u_context*);

/**
 * @brief register a newly logged user  
 * @return TRUE on success, FALSE if already registered 
 */
boolean u_register(struct u_context*, uint32_t, char*, time_t);


/**
 * @brief verify the list of programs checked in by user against the value stored in the database
 * @return TRUE if the new list matches the value supplied at registration 
 */
boolean u_verify(struct u_context*, uint32_t, char*);

/**
 * @brief call to print users who failed to meet their deadlines for updates
 */
void u_checkpoint(struct u_context*);

#endif
