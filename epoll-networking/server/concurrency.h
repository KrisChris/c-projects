/**
 * @file concurrency.h
 * @author Krystian Krol
 * @brief header file for concurrency related functions
 */
#ifndef CONCURRENCY_H
#define CONCURRENCY_H
#include <sys/epoll.h>
#include <fcntl.h>
#include "common.h"

/* serve no more than THIS number at once */
#define MAX_EVENTS 4

/**
 * @brief with references to globally used buffers and file descriptors
 */
struct e_context{
  int epfd; /**< epoll file descriptor */
  int lfd; /**< listening socket file descriptor */
  struct epoll_event evlist[MAX_EVENTS]; /**< event list of active fds returned by epoll_wait */
  khash_t(32) *fd_buf; /**< map from open file descriptors to buffers */
  khash_t(64) *fd_off; /**< map from open file descriptors to buffer offsets */
};

/**
 * @brief sets default values to e_context struct
 *  to set epoll fd inside the e_context, 
 *  @return -1 on error
 *  */
int e_init(struct e_context *context);

/** @brief add a fd to the list of epolled fds
 *  @fd to be registed for listening
 *  @return -1 on error
 * */
int e_add(struct e_context *context, int fd);

/** @brief user logging out cleanup
 *  @fd a dead fd for which to free resources
 */
void e_drop(struct e_context *context, int fd);

/** @brief retrieve a buffered message if it's ready
 *  @f function called to determine if the buffer contains a valid protocol message
 *  @consumer pointer to a buffer that shall consume the message
 *  @return TRUE if the consumer was modified 
 * */
boolean get_buff(struct e_context *context, int fd, boolean (*f)(char*, size_t), char *consumer, size_t len);

/** @brief  buffer up the data in the pointer
 *  
 */
void e_buff(struct e_context*, int, char*,size_t);

/** @brief blocks until one or any sockets become active , return number of served at a time
 *  @f a payload function that shall be executed for every active fd that is not a listening fd
 *  @return a number of clients served on this call
 */
int handle_active_clients(struct e_context *context, void (*f)(void*, int), void*, int);

#endif /* CONCURRENCY_H */
