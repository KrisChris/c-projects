/**
 * @file networking.c
 * @author Krystian Krol
 * @brief contains implementations of interfaces declared in networking.h
 */
#define _BSD_SOURCE 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include "common.h"
#include "networking.h"

int
net_connect(const char *host, const char *service, int type){
  struct addrinfo hints;
  struct addrinfo *result, *rp;
  int sfd, s;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_canonname = NULL;
  hints.ai_addr = NULL;
  hints.ai_next = NULL;
  hints.ai_family = AF_UNSPEC; /* IPv4 albo IPv6 */
  hints.ai_socktype = type;

  s = getaddrinfo(host, service, &hints, &result);
  if ( s != 0) {
    errno = ENOSYS;
    return -1;
  }

  for (rp = result; rp != NULL; rp = rp->ai_next) {
    sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    if (sfd == -1)
      continue; /* próbujemy z kolejnym adresem */

    if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
      break; 

    close(sfd); /* zamykamy gniazdo i próbujemy znowu */
  }

  freeaddrinfo(result);

  return (rp == NULL) ? -1 : sfd;
 } /* net_connect */


static int        
net_passive_socket(const char *service, int type, socklen_t *addrlen,
                  boolean doListen, int backlog)
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd, optval, s;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;
    hints.ai_socktype = type;
    hints.ai_family = AF_UNSPEC;       
    hints.ai_flags = AI_PASSIVE;    

    s = getaddrinfo(NULL, service, &hints, &result);
    if (s != 0)
        return -1;

    optval = 1;
    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1)
            continue;                   

        if (doListen) {
            if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &optval,
                    sizeof(optval)) == -1) {
                close(sfd);
                freeaddrinfo(result);
                return -1;
            }
        }

        if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
            break;                      /* Success */

     
        close(sfd);
    }

    if (rp != NULL && doListen) {
        if (listen(sfd, backlog) == -1) {
            freeaddrinfo(result);
            return -1;
        }
    }

    if (rp != NULL && addrlen != NULL)
        *addrlen = rp->ai_addrlen;     

    freeaddrinfo(result);

    return (rp == NULL) ? -1 : sfd;
} /* net_passive_socket */

int
net_listen(const char *service, int backlog, socklen_t *addrlen)
{
    return net_passive_socket(service, SOCK_STREAM, addrlen, TRUE, backlog);
} /*net_listen */


int
net_bind(const char *service, int type, socklen_t *addrlen)
{
    return net_passive_socket(service, type, addrlen, FALSE, 0);
}

boolean set_blocking(int fd, boolean blocking)
{
   if (fd < 0) 
     return FALSE;
   int flags = fcntl(fd, F_GETFL, 0);
   if (flags < 0) 
     return FALSE;
   flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);
   return (fcntl(fd, F_SETFL, flags) == 0) ? TRUE : FALSE;
} /* set_blocking */
