/** 
 *  @file	proc.c
 *  @brief	Funkcje związane z wyciąganiem danych o istniejących procesach.
 */
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include "proc.h"

/*! \brief process_name
 *         Na podstawie id procesu wyciąga jego nazwę z pliku /proc/(id)/status.
 */
char* process_name(int id) {
  char* ret;
  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  asprintf(&ret, "/proc/%d/status", id);
  if (ret == NULL) {
    syslog(LOG_INFO,  "Error in asprintf\n");
  }
  fp = fopen(ret, "r");
  free(ret);
  if (fp == NULL) {
    exit(EXIT_FAILURE);
  } else {
    if((read = getline(&line, &len, fp)) != -1) {
      sscanf(line, "Name:\t%s", line);
    }
    fclose(fp);
  }
  return line;
}
/*! \brief proc
 *         Funkcja zwraca do 'char** proces' ciąg nazw istniejących procesów w systemie.
 */
int proc(char** proces) {
  DIR *dp;
  struct dirent *ep;
  int val;
  char* name;
  asprintf(proces, "");
  dp = opendir ("/proc");
  if (dp != NULL)
  {
    while ((ep = readdir (dp))) {
      val = atoi(ep->d_name);
      // syslog(LOG_INFO,  "%d\n", val);
      if(val > 0) {
        name = process_name(val);
        // syslog(LOG_INFO,  "%s ", name);
        asprintf(proces, "%s %s", *proces, name);
        free(name);
      }
    }
    (void) closedir (dp);
  }
  else
    perror ("Couldn't open the directory");
  return 0;
}
