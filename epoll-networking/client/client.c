/** 
 *  @file	client.c
 *  @brief	Plik zawiera główny main oraz funkcje pomocne przy komunikacji z serwerem.
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <syslog.h>
#include "network.h"
#include "proc.h"

#define BUF_SIZE 50
#define ROK "rok\n"
#define UOK "uok\n"

int send_r(int fd, int id, int tim);
int update_loop(int fd, int id, int tim);
/*! \brief send_r
 *         Funkcja wysyła do serwera komunikat o rejestracji nowego klienta wraz obecną listą uruchomionych procesów.
 */
int send_r(int fd, int id, int tim) {
  char *all;
  char *msg;
  char response[BUF_SIZE];
  proc(&all);
  asprintf(&msg, "r %d %d %s\n", id, tim, all);
  write(fd, msg, strlen(msg));
  free(msg);
  read(fd, response, BUF_SIZE);
  if(strcmp(response, ROK) == 0)
    return 0;
  return -1;
}
/*! \brief update_loop
 *         Funkcja w pętli wysyła do serwera aktualny stan procesów z częstotliwością 'tim' sekund.
 */
int update_loop(int fd, int id, int tim) {
  char *all;
  char *msg;
  char response[BUF_SIZE];
  proc(&all);
  asprintf(&msg, "u %d %s\n", id, all);
  // fprintf(stderr, "%s", msg);
  fflush(stdout);
  sleep(tim);
  write(fd, msg, strlen(msg));
  free(msg);
  read(fd, response, BUF_SIZE);
  if(strcmp(response, UOK) == 0) {
    // syslog(LOG_ERR, "UPDATE COMPLETE\n");
  } else {
    syslog(LOG_ERR, "ERROR: %s\n", response);
  }
  return update_loop(fd, id, tim);;
}
/*! \brief main
 *         Główna funkcja programu.
 */
int main(int argc, char **argv) {
  openlog("client", LOG_PID|LOG_CONS, LOG_USER);
  srand(time(NULL));
  int sfd;
  int id = rand();
  if (argc < 3) {
    syslog(LOG_ERR, "Usage: %s host port time\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  sfd = client_connect(argv[1], argv[2]);
  int tim = atoi(argv[3]);
  fprintf(stderr, "time: %d\n", tim);
  // daemon(1, 1);
  if(sfd) {
    if(send_r(sfd, id, tim) == 0) {
      syslog(LOG_ERR, "REGISTER COMPLETE\n");
      update_loop(sfd, id, tim);
    }
  }
  closelog();
  exit(EXIT_SUCCESS);
}
