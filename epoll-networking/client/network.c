/** 
 *  @file	network.c
 *  @brief	Funkcje związane ustawienie połączenia z serwerem.
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include "network.h"

/*! \brief client_connect
 *         Na podstawie argumentów host oraz port klient łączy się ze zdalnym serwerem.
 *	   Zwraca nam deskryptor nawiązanego połączenia. W przypadku nieudanego połączenia przerywa działanie programu.
 */
int client_connect(char *host, char* port) {
  int sfd, s;
  struct addrinfo hints;
  struct addrinfo *result, *rp;
  /* Obtain address(es) matching host/port */

   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
   hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
   hints.ai_flags = 0;
   hints.ai_protocol = 0;          /* Any protocol */

   s = getaddrinfo(host, port, &hints, &result);
   if (s != 0) {
       syslog(LOG_INFO,  "getaddrinfo: %s\n", gai_strerror(s));
       exit(EXIT_FAILURE);
   }

   for (rp = result; rp != NULL; rp = rp->ai_next) {
       sfd = socket(rp->ai_family, rp->ai_socktype,
                    rp->ai_protocol);
       if (sfd == -1)
           continue;

       if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1) {
          syslog(LOG_INFO,  "Success connect\n");
           break;                  /* Success */
       }
       close(sfd);
   }

   if (rp == NULL) {               /* No address succeeded */
       syslog(LOG_INFO,  "Could not connect\n");
       exit(EXIT_FAILURE);
   }
   freeaddrinfo(result);           /* No longer needed */

   return sfd;
}
