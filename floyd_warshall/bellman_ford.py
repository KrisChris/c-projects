import zmq
import time
import random
import threading
import sys
import ast
from multiprocessing import Lock
import operator

l = Lock()
number = 12000 # socket ports will be used starting from number until number + n 
global_n = 20 # size of graph
p = 0.05 # probability of generating a link between nodes in a graph
steps = 100 # how many steps of the Bellman-Ford messaging to do

class Node:
    def __init__(self, mynum, nodes, n = global_n,  start = number, timeout = 100):
        self.start = start # start range for the socket port
        self.nodes = nodes.copy() # stores the nodes connected to this node
        self.mynum = mynum+start
        self.nexthop = {h:h for h in self.nodes.keys()}
        self.context = zmq.Context() # initiates the zmq machinery
        self.consumer = self.context.socket(zmq.SUB)
        self.consumer.RCVTIMEO = timeout # important to avoid inifinite waiting
        # this node is a producer at its mynum port
        self.producer = self.context.socket(zmq.PUB)
        self.producer.bind("tcp://*:%d"%(self.mynum)) 
        self.n = n
        
    # this node is talking via ZeroMQ only to the closest (hops = 1) nodes
    def subscribe(self):
        for k, v in self.nodes.iteritems():
            if v != self.n+1:
                self.consumer.connect("tcp://localhost:%d"%k)
                self.consumer.setsockopt(zmq.SUBSCRIBE, str(k))

    def choosebest(self, sender, k, v):
        if v + 1 < self.nodes[k]:
            self.nodes[k] = v + 1
            self.nexthop[k] = sender

    # that's a distributed Bellman-Ford
    def update_step(self, sender, msg):
        for k, v in msg.iteritems():
            if k in self.nodes:
                self.choosebest(sender, k, v)
            else:
                self.nexthop[k] = sender # a new path to k through sender
                self.nodes[k] = v + 1 
        self.nodes[self.mynum] = n + 1
                
    # run a distributed Bellman Ford to compute the shortest paths
    def do(self):
        self.subscribe()
        r = 1
        for step in xrange(steps):
            self.producer.send("%d=%s" % (self.mynum, self.nodes))
            for updates in xrange(r):
                try:
                    msg = self.consumer.recv()
                    # receiving a vector of distances from one of neighbours
                    topic, msg = msg.split("=")
 		    r = r +1
                    msg =  ast.literal_eval(msg) # a way to cast string "{key:val}" to a dict
                    self.update_step(int(topic), msg)
                except: # possible timeout
                    r = r -1
                    pass
        # locking is necessary to print correctly
        l.acquire()
        print "Node: ", self.mynum, "finished"
        #print sorted(self.nodes.items(), key = operator.itemgetter(0)), self.nexthop
        print ""
        l.release()
                
            
# assumes that if x is linked to y, y is linked to x
def getgraph(n, p):
    graph = [{number+i: n+1} for i in range(n)]
    for i in range(n):
        for j in range(n):
            if random.random() < p:
                graph[i][number+j] = 1
                graph[j][number+i] = 1
        graph[i][number+i] = n+1
    return graph
                
    
# will be passed to the thread
def thing(node):
    node.do()

graph = getgraph(global_n, p)

working_set = {Node(mynum = i, nodes = graph[i]) for i in range(global_n)}

# we allow all Node objects to safely set up their publishers.
time.sleep(5)

# I deploy nodes on separate threads, but they can be deployed as separate processes
# or even on separate machines. 
threads = [threading.Thread(target = thing, args = (node,)) for node in working_set]

for t in threads:
    t.start()

