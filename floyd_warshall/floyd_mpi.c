/** Compiles with -std=gnu11; the number of graph nodes ( n) must be divisible
 * by
 * the number avaliable processing nodes in the cluster */


#include "mpi.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>



void gen_graph(int n, double p, int* l, int myrank, int notasks, MPI_Comm comm);
void print_graph(int n, int* l, int myrank, int notasks, MPI_Comm comm);
void floyd(int n, int* l, int myrank, int notasks, MPI_Comm comm);
void copy_row(int* local_mat, int n, int p, int* row_k, int k);
int owner(int k, int p, int n);

static inline int min(int a, int b) {
  return a < b ? a : b;
} /* min */ 

static inline void infinitize(int n, int* l) {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      if (l[i*n+j] == 0)
	l[i*n+j] = n + 1;
    }
    l[i*n+i] = 0;
  }
}

static inline void deinfinitize(int n, int* l) {
  for (int i = 0; i < n*n; ++i)
    if (l[i] == n+1)
      l[i] = 0;
}

const char* usage =
"path.x -- All-pairs shortest path on a random graph\n"
"Flags:\n"
" - n -- number of nodes (200)\n"
" - p -- probability of including edges (0.05)\n";

int main(int argc, char **argv) {
  int n = 20; // Number of nodes
  double p = 0.05; // Edge probability
  int* l; // local matrix
  int myrank;
  int notasks;
  MPI_Comm comm;
  const char* optstring = "hn:p:";
  int c;
  while ((c = getopt(argc, argv, optstring)) != -1) {
    switch (c) {
    case 'h':
      fprintf(stderr, "%s", usage);
      return -1;
    case 'n': n = atoi(optarg); break;
    case 'p': p = atof(optarg); break;
    }
  }

  MPI_Init(&argc, &argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_size(comm, &notasks);
  MPI_Comm_rank(comm, &myrank);
  l = malloc(n*n/notasks * sizeof(int));
  gen_graph(n, p, l, myrank, notasks, comm);
  print_graph(n, l, myrank, notasks, comm);

  
 
  floyd(n, l, myrank, notasks, comm);
 

  if (myrank == 0) printf("The solution: \n");
  print_graph(n, l, myrank, notasks, comm);
  
  free(l);
  MPI_Finalize();

  return 0;
} /* main */

void gen_graph(int n, double p, int* l, int myrank, int notasks, MPI_Comm comm) {
  int* temp = NULL;

  if (myrank == 0) {
    temp = malloc(n*n * sizeof(int));
    srand(time(NULL));
    for (int j = 0; j < n; ++j) {
      for (int i = 0; i < n; ++i)
	temp[j*n+i] = (((double)rand()/RAND_MAX) < p) ;
      temp[j*n+j] = 0; // no self cycles 
    }
    infinitize(n, temp);
    MPI_Scatter(temp, n*n/notasks, MPI_INT, l, n*n/notasks, MPI_INT, 0, comm);
    free(temp);
    }else {
      MPI_Scatter(temp, n*n/notasks, MPI_INT, l, n*n/notasks, MPI_INT, 0, comm);
    }
} /* gen_graph */

void print_graph(int n, int* l, int myrank, int notasks, MPI_Comm comm) {
  
  int* temp = NULL;

  if (myrank == 0) {
    temp = malloc(n*n * sizeof(int));
    MPI_Gather(l, n*n/notasks, MPI_INT, temp, n*n/notasks, MPI_INT, 0, comm);
    for(int i = 0; i < n; i++) { 
      for(int j = 0; j < n; j++) {
	printf("%d ", temp[i*n + j]);
      }
      printf("\n");
    }
    free(temp);
  } else {
    MPI_Gather(l, n*n/notasks, MPI_INT, temp, n*n/notasks, MPI_INT, 0, comm);
  }
} /* print_graph */

void floyd(int n, int* l, int myrank, int notasks, MPI_Comm comm) {
  int global_k, local_i, global_j, temp;
  int root;
  int* row_k = malloc(n*sizeof(int));
  
  for (global_k = 0; global_k < n; global_k++) {
    root = owner(global_k, notasks, n);
    if (myrank == root)
      copy_row(l, n, notasks, row_k, global_k);
    MPI_Bcast(row_k, n, MPI_INT, root, comm);
    for (local_i = 0; local_i < n/notasks; local_i++){
      if (l[local_i*n + global_k] == n +1) continue;
      for (global_j = 0; global_j < n; global_j++) {
	temp = l[local_i*n + global_k] + row_k[global_j];
	if (temp < l[local_i*n+global_j])
	  l[local_i*n + global_j] = temp;
      }
    }
   }
   free(row_k);
   deinfinitize(n, l);
} /* floyd */

void copy_row(int* local_mat, int n, int p, int* row_k, int k) {
   int j;
   int local_k = k % (n/p);

   for (j = 0; j < n; j++)
      row_k[j] = local_mat[local_k*n + j];
}  /* copy_row */

int owner(int k, int p, int n) {
   return k/(n/p);
}  /* owner */
