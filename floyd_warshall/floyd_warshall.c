// Autor: Sławomir Krystian Król, 14308
/* compiled with: gcc graph_seq.c -lgomp -pthread -o graph.x -std=gnu11 -fopenmp */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <omp.h>
#include <pthread.h>

int* gen_graph(int n, double p);
void write_matrix(const char* fname, int n, int *a);
/* sequential version */
int* shortest_paths_seq(int n, int* restrict l);
void print_path(int n, const int* const restrict l, int u, int v, int c);

/* declarations of functions using OpenMP */
int square(int n, int* restrict l, int* restrict lnew);
/* that's a very slow OpenMP version */
void shortest_paths_omp_n3logn(int n, int* restrict l);


/* this one is about 2 times faster than the sequential version, using
   8 cores on my machine :( */
int* shortest_paths_omp_mmult(int n, int* restrict l); 


/* pthread stuff starts here */

int* shortest_paths_pthreaded(int n, int* restrict l);


/* represents a chunk of total work assigned for each thread */
struct work_unit {
  int* restrict l;
  int* restrict d; 
  pthread_barrier_t* barrier; /* shared by no_thr threads */
  pthread_barrier_t* barrier_main; /* blocking the main thread until workers finish */
  int start, end; /* the bounds for the loop */
  int n;
  int running; /* provides a way to cancel the task */
 
};

static inline void* thread_payload(void* parg);


static inline void* memcheck(void* const restrict p) {
  if(p)
    return p;

  fprintf(stderr, "\n\r memory allocation error \n\r");
  exit(1);
}

static inline void infinitize(int n, int* l)
{
  for (int i = 0; i < n*n; ++i)
    if (l[i] == 0)
      l[i] = n+1;
}

static inline void deinfinitize(int n, int* l)
{
  for (int i = 0; i < n*n; ++i)
    if (l[i] == n+1)
      l[i] = 0;
}


const char* usage =
"path.x -- All-pairs shortest path on a random graph\n"
"Flags:\n"
" - n -- number of nodes (200)\n"
" - p -- probability of including edges (0.05)\n"
" - i -- file name where adjacency matrix should be stored (none)\n"
" - o -- create output files ('outseq' for seq alg and 'outomp' for omp)\n"
" - f -- point on the XY plane from which the path will be calculated (3)\n"
" - t -- point on the XY plane to which the path will be calculated (7)\n";

int main(int argc, char** argv)
{
  int n = 200; // Number of nodes
  double p = 0.05; // Edge probability
  const char* ifname = NULL; // Adjacency matrix file name
  const char* ofseq = "outseq"; // Path matrix file name
  const char* ofomp = "outomp";
  int create_output = 0; // default: do not create output
  int f = 3; // Showing the path for a chosen pair of vertices
  int t = 7;
  // Option processing
  extern char* optarg;
  const char* optstring = "hon:d:p:i:f:t:";
  int c;
  while ((c = getopt(argc, argv, optstring)) != -1) {
    switch (c) {
    case 'h':
      fprintf(stderr, "%s", usage);
      return -1;
    case 'n': n = atoi(optarg); break;
    case 'p': p = atof(optarg); break;
    case 'o': create_output = 1; break;
    case 'i': ifname = optarg; break;
    case 'f': f = atoi(optarg); break;
    case 't': t = atoi(optarg); break;
    }
  }
  if (f >= n || t >= n) {
    fprintf(stderr, "Path from %d to %d won't exist "
    "in a graph of size %d\n", f, t, n);
    fprintf(stderr, "Consider generating a bigger graph or changing"
    " the default f and t parameters\n");
    return -1;
  }	
  // Graph generation + output
  int *lseq = gen_graph(n, p);
  int* lomp = (int*) memcheck(calloc(n*n, sizeof(int)));
  int* lposix = (int*) memcheck(calloc(n*n, sizeof(int)));
  memcpy(lomp, lseq, n*n * sizeof(int));
  memcpy(lposix, lseq, n*n * sizeof(int));

  if (ifname)
    write_matrix(ifname, n, lseq);
  

  // Run the algorithms .. 
  double t0 = omp_get_wtime();
  /* l is modified, will store the shortest paths; dseq
   * is storing the costs from every x to y */
  int* dseq = shortest_paths_seq(n, lseq);
  double t1 = omp_get_wtime();  
  printf("== Sequential took: %g\n", t1-t0);

  
  /* 
  int* lcopy = (int*) memcheck(calloc(n*n, sizeof(int)));
  memcpy(lcopy, lomp, n*n * sizeof(int));
  t0 = omp_get_wtime();
  //shortest_paths_omp_n3logn(n, lcopy); 
  t1 = omp_get_wtime();
  printf("OpenMP n3logn took: %g\n", t1-t0);
  */
  printf("== OpenMP with %d threads\n", omp_get_max_threads());

  t0 = omp_get_wtime();
  int* domp = shortest_paths_omp_mmult(n, lomp); 
  t1 = omp_get_wtime();
  printf("OpenMP simple matrix mult took: %g\n", t1-t0);

  printf("== Posix threads with %ld threads \n",  sysconf( _SC_NPROCESSORS_ONLN ));
  t0 = omp_get_wtime();
  int* dposix = shortest_paths_pthreaded(n, lposix); 
  t1 = omp_get_wtime();
  printf("Pthreads with thread reuse took: %g\n", t1-t0);


  // Print a chosen path, these three should print identical paths.
  if (lseq[f*n+t]) {
    print_path(n, lseq, f, t, dseq[f*n+t]);
    print_path(n, lomp, f, t, domp[f*n+t]);
    print_path(n, lposix, f, t, dposix[f*n+t]);
  } else {
    printf("no path from %d to %d \n", f, t);
  }
 
  
  for(int i = 0; i < n*n; ++i){
    if(lomp[i] != lseq[i]) {
       printf("OMP inconsistent at %d, %d vs %d \n", i, lomp[i], lseq[i]);
       break;
    }
    if(lposix[i] != lseq[i]) {
       printf("POSIX inconsistent at %d, %d vs %d \n", i, lposix[i], lseq[i]);
       break;
    }
  } 
  
  // Generate output files - writing "paths"
  if (create_output) {
    write_matrix(ofseq, n, lseq);
    write_matrix(ofomp, n, lomp);
  }

  // Clean up
  free(lseq);
  free(lomp);
  free(lposix);
  free(dseq);
  free(domp);
  free(dposix);
  
  return 0;
}


int* gen_graph(int n, double p)
{
  int* l = memcheck(calloc(n*n, sizeof(int)));
  srand(time(NULL));

  for (int j = 0; j < n; ++j) {
    for (int i = 0; i < n; ++i)
      l[j*n+i] = (((double)rand()/RAND_MAX) < p);
    l[j*n+j] = 0;
  }
  return l;
}


void write_matrix(const char* fname, int n, int* a)
{
  FILE* fp = fopen(fname, "w+");
  if (fp == NULL) {
    fprintf(stderr, "Could not open output file: %s\n", fname);
    exit(-1);
  }
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j)
      fprintf(fp, "%d ", a[j*n+i]);
    fprintf(fp, "\n");
  }
  fclose(fp);
}



void print_path(int n, const int* const restrict l, int u, int v, int c)
{
  int* stack = calloc(c+1, sizeof(int));
  stack[c] = v;
  int i = c;
  for(int e = l[u*n+v]; e != n+1; e = l[u*n+e]){
    stack[--i] = e;
  }
  printf("Path from %d to %d at cost %d: ", u, v, c);
  for(int i = 0; i <= c; i++)
    printf("%d ", stack[i]);

  free(stack);
  printf("\n\r");
}


/* this one will return just the shortest paths. 
 */
int* shortest_paths_omp_mmult(int n, int* restrict l){
 int* restrict d = memcheck(calloc(n*n, sizeof(int)));
 
for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      if (l[i*n+j]) {
	d[i*n+j] = 1;
	l[i*n+j] = i;
      } else {
         // Infinitize
	d[i*n+j] = n+1;
        l[i*n+j] = n+1;
      }
    }
    d[i*n+i] = 0;        
  }
  

 int i, v, w;
 

 {
 for ( i = 0; i < n; i++) {

   #pragma omp parallel for schedule(static) shared(d,l,n,i) private(w)
    for ( v = 0;  v < n; v++) {
      if (l[v*n+i] == n+1) continue;
      for ( w = 0; w < n; w++) {
	if (d[v*n+w] > (d[v*n+i] + d[i*n+w])) {
	  d[v*n+w] = d[v*n+i] + d[i*n+w];
	  l[v*n+w] = l[i*n+w]; // Memorizes the path from x to y
	}
      }
    }
  }
 }
 



/*
here: testing variant with memory alignment. 
int step = 100;
  for(int k = 0; k < n; ++k) {
   {
#pragma omp parallel for shared(k, l, n, d, step)
   for(int j = 0; j < n; j += step) {
     for (int i = 0; i < n; i += step) {
       for( int jj = j; jj < j + step; ++jj) {
	 for(int ii = i; ii < i + step; ++ii) {
	   if ( (d[ii*n+k]+d[k*n+jj]) < d[ii*n+jj]){
	     d[ii*n+jj] = d[ii*n+k]+d[k*n+jj];
	     l[ii*n+jj] = l[k*n+jj];
	   }
	 }
       }
     }
   }
   }}
*/

 // Cleanup
  
 //  free(dist_to);
  deinfinitize(n, d);
  return d; // Returns all distances
}



// Mutates data pointed to by *l, not a pure function!
int* shortest_paths_seq(int n, int* restrict l)
{
  int* restrict dist_to = memcheck(calloc(n*n, sizeof(int)));

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      if (l[i*n+j]) {
	dist_to[i*n+j] = 1;
	l[i*n+j] = i;
      } else {
         // Infinitize
	dist_to[i*n+j] = n+1;
        l[i*n+j] = n+1;
     }
    }
    dist_to[i*n+i] = 0;        
  }

 for (int i = 0; i < n; i++) {
    for (int v = 0;  v < n; v++) {
      if(v == i) continue;
      if (l[v*n+i] == n+1) continue;
      for (int w = 0; w < n; w++) {
	if (dist_to[v*n+w] > (dist_to[v*n+i] + dist_to[i*n+w])) {
	  dist_to[v*n+w] = dist_to[v*n+i] + dist_to[i*n+w];
	  l[v*n+w] = l[i*n+w]; // Memorizes the path from x to y
	}
      }
    }
  }
  
 // Cleanup
  deinfinitize(n, dist_to);

  return dist_to; // Returns all distances
}


int square(int n, int* restrict l, int* restrict lnew)
{
  int done = 1;
 
  #pragma omp parallel for shared(l, lnew) reduction(&& : done)
   for (int i = 0; i < n; i++) {
    for (int v = 0;  v < n; v++) {
      if (l[v*n+i] == n+1) continue;
      for (int w = 0; w < n; w++) {
	if (l[v*n+w] > (l[v*n+i] + l[i*n+w])){
	  lnew[v*n+w] = l[v*n+i] + l[i*n+w];
	  done = 0;
	}
      }
    }
  }
  return done;
}

/* in this variant, different omp features are used,
 this one is slower, n^3*log(n) complexity  */
void shortest_paths_omp_n3logn(int n, int* restrict l)
{
  infinitize(n, l);
  for (int i = 0; i < n*n; i += n+1)
    l[i] = 0;

  // We square the matrix log n times, until two successive matrices
  // are the same
  int* restrict lnew = (int*) memcheck(calloc(n*n, sizeof(int)));
  memcpy(lnew, l, n*n * sizeof(int));
  for (int done = 0; !done; ) {
    done = square(n, l, lnew); // function square is OpenMP
    memcpy(l, lnew, n*n * sizeof(int));
  }
  free(lnew);
  deinfinitize(n, l);
}


int* shortest_paths_pthreaded(int n, int* restrict l) {
  
  int* restrict d = memcheck(calloc(n*n, sizeof(int)));
 
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      if (l[i*n+j]) {
        d[i*n+j] = 1;
	l[i*n+j] = i;
      } else {
      // Infinitize
        d[i*n+j] = n+1;
        l[i*n+j] = n+1;
      }
    }
    d[i*n+i] = 0;        
  }
  int no_thr = sysconf( _SC_NPROCESSORS_ONLN );
 
  int chunk = n / no_thr;
  
  pthread_t threads[no_thr];
  
  pthread_barrier_t bar, bar_main;
  pthread_barrier_init(&bar, NULL, no_thr);
  pthread_barrier_init(&bar_main, NULL, 2);
  
  struct work_unit* units = malloc(no_thr  * sizeof(struct work_unit));

  
  int c = 0;
  for (int i = 0; i < no_thr; ++i) {
    units[i].n = n;
    units[i].barrier = &bar;
    units[i].barrier_main = &bar_main;
    units[i].l = l;
    units[i].d = d;
    units[i].start = c;
    c+=chunk;
    units[i].end = c;
    units[i].running = 1;
  }
  units[no_thr-1].end = n;
  

  /* this solution is faster than OpenMP because the no_thr number of 
   * threads is created EXACTLY once  and they are reused with every 
   * iteration of i loop */
  
  for (int j = 0; j < no_thr; ++j) {
    pthread_create(&threads[j], NULL, thread_payload, &units[j]);
  }
  
  pthread_barrier_wait(&bar_main);
   
  pthread_barrier_destroy(&bar);
  pthread_barrier_destroy(&bar_main);
  deinfinitize(n, d);
  free(units);
  return d; // Returns all distances
}

/* that's the inner two loops of the Floyd-Warshall N^3 algo
 * that shall be passed to the thread
 */
static inline void* thread_payload(void* parg) {
  struct work_unit* p = (struct work_unit*)parg;
  int n = p->n;
  int start = p->start, end = p->end;
  int i = 0;
  int* restrict d = p->d;
  int* restrict l = p->l;
  int last = 0;
  while( i < n && p->running) {
    for (int v = start;  v < end; v++) {
      if (l[v*n+i] == n+1) continue;
      for ( int w = 0; w < n; w++) {
	if (d[v*n+w] > (d[v*n+i] + d[i*n+w])) {
	  d[v*n+w] = d[v*n+i] + d[i*n+w];
	  l[v*n+w] = l[i*n+w]; 
	}
      }
    }
    last = pthread_barrier_wait(p->barrier);
    i++;
  }
  /* the last thread to finish */
  if (last)
    pthread_barrier_wait(p->barrier_main);
  return 0;
}
